package ru.specialist.spring.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import ru.specialist.spring.entity.Post;
import ru.specialist.spring.entity.Tag;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Getter
@Setter
public class PostDto {

    private Long id;
    private String title;
    private String content;
    private String tags;
    private String author;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dtCreated;

    @JsonInclude(NON_NULL)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dtUpdated;

    public static PostDto fromPost(Post post){
        PostDto postDto = new PostDto();
        postDto.setId(post.getPostId());
        postDto.setTitle(post.getTitle());
        postDto.setContent(post.getContent());
        postDto.setTags(post.getTags().stream()
                .map(Tag::getName)
                .collect(Collectors.joining(" ")));
        postDto.setAuthor(post.getUser().getUsername());
        postDto.setDtCreated(post.getDtCreated());
        postDto.setDtUpdated(post.getDtUpdated());
        return postDto;

        //15:57
    }
}
