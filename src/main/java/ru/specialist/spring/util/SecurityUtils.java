package ru.specialist.spring.util;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import ru.specialist.spring.entity.Post;
import ru.specialist.spring.entity.Role;
import ru.specialist.spring.entity.User;

import java.util.Objects;
import java.util.stream.Collectors;

public class SecurityUtils {

    public static final String ACCESS_DENIED = "Access Denied";

    public static UserDetails getCurrentUserDetails() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        if (!(principal instanceof UserDetails)) {
            throw new AccessDeniedException(ACCESS_DENIED);
        }

        return (UserDetails) principal;

    }

    public static void checkAuthorityOnPost(Post post) {
        UserDetails user = getCurrentUserDetails();
        if (!isAuthor(user, post)) {
            throw new AccessDeniedException(ACCESS_DENIED);
        }
    }

    public static boolean isAuthor(UserDetails user, Post post) {
        return Objects.equals(user.getUsername(), post.getUser().getUsername());
    }

    public static void checkIsAuthorOrAdmin(Post post) {
        UserDetails user = getCurrentUserDetails();
        if (!isAuthor(user, post) && !isAdmin(user)) {
            throw new AccessDeniedException(ACCESS_DENIED);
        }
    }

    private static boolean isAdmin(UserDetails user) {
        return user.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet())
                .contains("ROLE_" + Role.ADMIN);
    }
}
