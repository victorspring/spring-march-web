package ru.specialist.spring.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class RestBasictAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

/*    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authEx)
            throws IOException {
        response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        PrintWriter writer = response.getWriter();
        String result = new ObjectMapper().writeValueAsString(
                Map.of("error", "HTTP Status 401", "message", authEx.getMessage()));
        writer.print(result);
    }*/

    @Override
    public void afterPropertiesSet()  {
        setRealmName("Specialist");
        super.afterPropertiesSet();
    }

}
