package ru.specialist.spring.service;

import ru.specialist.spring.dto.PostDto;
import ru.specialist.spring.entity.Post;

import java.util.List;

public interface PostService {
    Post create(PostDto postDto);

    Post findById(long postId);

    Post update(long postId, PostDto postDto);

    void delete(long postId);

    List<Post> findAll(String query);
}
