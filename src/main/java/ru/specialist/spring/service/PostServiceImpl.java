package ru.specialist.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.specialist.spring.dto.PostDto;
import ru.specialist.spring.entity.Post;
import ru.specialist.spring.entity.Tag;
import ru.specialist.spring.repository.PostRepository;
import ru.specialist.spring.repository.TagRepository;
import ru.specialist.spring.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.specialist.spring.controller.PostController.SORT_DT_CREATED;
import static ru.specialist.spring.util.SecurityUtils.*;

@Service
@Transactional
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final TagRepository tagRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository,
                           UserRepository userRepository,
                           TagRepository tagRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.tagRepository = tagRepository;
    }

    @Override
    @Secured("ROLE_USER")
    public Post create(PostDto postDto) {
        Post post = new Post();
        if (StringUtils.hasText(postDto.getTitle())) {
            post.setTitle(postDto.getTitle());
        }

        if (StringUtils.hasText(postDto.getContent())){
            post.setContent(postDto.getContent());
        }

        post.setDtCreated(LocalDateTime.now());
        UserDetails details = getCurrentUserDetails();
        post.setUser(userRepository
                .findByUsername(details.getUsername())
                .orElseThrow());
        post.setTags(parseTags(postDto.getTags()));

        return postRepository.save(post);
    }

    @Override
    public Post findById(long postId) {
        Post post = postRepository.findById(postId).orElseThrow();

        post.getComments().size();
        post.getTags().size();
        return post;
    }

    @Override
    @Secured("ROLE_USER")
    public Post update(long postId, PostDto postDto) {
        Post post = postRepository.findById(postId).orElseThrow();
        checkAuthorityOnPost(post);

        if (StringUtils.hasText(postDto.getTitle())){
            post.setTitle(postDto.getTitle());
        }

        if (StringUtils.hasText(postDto.getContent())){
            post.setContent(postDto.getContent());
        }

        if (postDto.getTags() != null){
            Set<Tag> newTags = parseTags(postDto.getTags());
            post.getTags().removeAll(newTags);
            removeUnusedTags(post);

            post.setTags(newTags);
        }

        post.setDtUpdated(LocalDateTime.now());
        return postRepository.save(post);
    }

    @Override
    public void delete(long postId) {
        Post post = postRepository.findById(postId).orElseThrow();
        checkIsAuthorOrAdmin(post);
        postRepository.deleteById(postId);
    }

    @Override
    public List<Post> findAll(String query) {
        List<Post> posts;
        if (StringUtils.hasText(query)){
            posts = postRepository.findByContentContainingIgnoreCase(query, SORT_DT_CREATED);
        } else {
            posts = postRepository.findAll(SORT_DT_CREATED);
        }

        posts.forEach(p -> p.getTags().size());
        return posts;
    }

    private void removeUnusedTags(Post post) {
        Set<Tag> tags = post.getTags().stream()
                .filter(t -> t.getPosts().size() == 1)
                .collect(Collectors.toSet());
        tagRepository.deleteAll(tags);
    }

    private Set<Tag> parseTags(String tags) {
        if (!StringUtils.hasText(tags)){
            return Collections.emptySet();
        }

        return Arrays.stream(tags.split(" "))
                .map(tagName -> tagRepository.findByName(tagName)
                        .orElseGet(() -> tagRepository.save(new Tag(tagName))))
                .collect(Collectors.toSet());
    }

}
