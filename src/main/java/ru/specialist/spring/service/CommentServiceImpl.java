package ru.specialist.spring.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.specialist.spring.entity.Comment;
import ru.specialist.spring.entity.Post;
import ru.specialist.spring.repository.CommentRepository;
import ru.specialist.spring.repository.PostRepository;
import ru.specialist.spring.repository.UserRepository;

import java.time.LocalDateTime;

import static ru.specialist.spring.util.SecurityUtils.getCurrentUserDetails;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final UserRepository userRepository;

    public CommentServiceImpl(PostRepository postRepository,
                              CommentRepository commentRepository,
                              UserRepository userRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void create(long postId, String content) {
        Comment comment = new Comment();
        comment.setContent(content);
        comment.setPost(postRepository
                .findById(postId)
                .orElseThrow());
        UserDetails details = getCurrentUserDetails();
        comment.setUser(userRepository
                .findByUsername(details.getUsername())
                .orElseThrow());
        comment.setDtCreated(LocalDateTime.now());

        commentRepository.save(comment);
    }

}
