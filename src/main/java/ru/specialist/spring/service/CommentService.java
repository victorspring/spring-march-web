package ru.specialist.spring.service;

public interface CommentService {
    void create(long postId, String content);
}
