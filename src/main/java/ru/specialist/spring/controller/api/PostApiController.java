package ru.specialist.spring.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.specialist.spring.dto.PostDto;
import ru.specialist.spring.service.PostService;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api/post")
public class PostApiController {

    private final PostService postService;

    @Autowired
    public PostApiController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping
    public ResponseEntity<List<PostDto>> findAll(@RequestParam(required = false) String query) {
        return ok(postService.findAll(query).stream()
                .map(PostDto::fromPost)
                .collect(Collectors.toList()));
    }


    @GetMapping("/{postId}")
    public ResponseEntity<PostDto> findById(@PathVariable long postId) {
        return ok(PostDto.fromPost(postService.findById(postId)));
    }

    @PostMapping()
    public ResponseEntity<PostDto> create(@RequestBody PostDto post) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(PostDto.fromPost(postService.create(post)));
    }

    @PutMapping("/{postId}")
    public ResponseEntity<PostDto> update(@PathVariable long postId,
                                          @RequestBody PostDto post) {
        return ok(PostDto.fromPost(postService.update(postId, post)));
    }

    @DeleteMapping("/{postId}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable long postId) {
        postService.delete(postId);
    }

}
